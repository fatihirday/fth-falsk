from flask import Flask, render_template
from routes import routes

app = Flask(__name__, template_folder='Templates')

method_list = [func for func in dir(routes) if callable(getattr(routes, func)) and not func.startswith("__")]

route = routes(app)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

