from sqlalchemy import *
from database import db, engine

User = Table('users', db,
    Column('id', Integer, primary_key=True),
    Column('name', String(120), nullable=False, index=True),
    Column('email', String(255), nullable=False),
)

db.create_all(engine)