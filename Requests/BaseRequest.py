from wtforms import Form
from flask import jsonify


class BaseRequest(Form):
    def errorMessage(self):
        return jsonify(error=dict(self.errors.items())), 200
