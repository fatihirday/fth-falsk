from Requests.BaseRequest import BaseRequest
from wtforms import StringField, validators


class TestRequest(BaseRequest):
    name = StringField('name', [
        validators.DataRequired(),
        validators.Length(min=4, max=25)
    ])
