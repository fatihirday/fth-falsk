import colorlog, logging

formatter = colorlog.ColoredFormatter(
    '\n%(log_color)s%(levelname)s (%(asctime)s): %(message)s %(reset)s\n',
    datefmt='%Y-%m-%d %H:%M:%S',
    reset=True,
    log_colors={
        'DEBUG': 'cyan',
        'INFO': 'blue',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'black,bg_white',
        'SUCCESS': 'green',
    },
    secondary_log_colors={},
    style='%'
)

logging.SUCCESS = 25
logging.addLevelName(logging.SUCCESS, 'SUCCESS')
logging.basicConfig(filename='logging.log', format='%(levelname)s (%(asctime)s): %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
handler = logging.StreamHandler()
handler.setFormatter(formatter)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)
logger.addHandler(handler)
setattr(logger, 'success', lambda message, *args: logger._log(logging.SUCCESS, message, args))