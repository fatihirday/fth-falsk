from Middlewares.Auth import Auth
from Controllers import HomeController


class routes:

    def __init__(self, app):
        @app.before_request
        def before_request():
            if Auth() is not None:
                return Auth()

        app.add_url_rule('/index/<name>', view_func=HomeController.index, methods=['GET'])
        app.add_url_rule('/home', view_func=HomeController.home, methods=['POST'])
        app.add_url_rule('/add-user', view_func=HomeController.addUser, methods=['POST'])
        app.add_url_rule('/get-user/<id>', view_func=HomeController.getUser, methods=['POST'])
        app.add_url_rule('/logs', view_func=HomeController.log, methods=['GET', 'POST'])
