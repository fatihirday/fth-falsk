from flask import make_response, request
from config import config


def Auth():
    if str(request.headers.get('Authorization')) != 'Bearer ' + str(config.auth_key):
        return make_response({'error': 'invalid auth'}, 401)
