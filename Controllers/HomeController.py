from flask import jsonify, render_template, request
from Requests.TestRequest import TestRequest
from database import engine
from Models.User import User
from logger import logger
from fthelper import *


def home():
    inputs = request.form
    form = TestRequest(inputs)
    if not form.validate():
        return form.errorMessage()

    return jsonify({'asd': inputs})


def index(name):
    return render_template('index.html', name=name)


def addUser():
    try:
        user = engine.execute(User.insert().values(name='fatih', email='fatihirdaygmail.com'))
        return jsonify({'success': {'id': user.lastrowid}})
    except Exception as e:
        return jsonify({'error': {'message': str(e)}})
        pass


def getUser(id):
    try:
        user = engine.execute(User.select().where(User.c.id == id)).fetchone()
        return jsonify({'User': dict(user)})
    except Exception as e:
        return jsonify({'error': {'message': str(e)}})


def log():
    logger.success('success message')
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warning message')
    logger.error('error message')
    logger.critical('critical message')
    return jsonify({'logs': 'OK'})
