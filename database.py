#!/usr/bin/python
from sqlalchemy import *
from config import config

engine = create_engine(
    config.db_connection + '+' + config.db_driver + '://' + config.db_username + ':' + config.db_passpord + '@' + config.db_host + ':' + config.db_port + '/' + config.db_database
)
conn = engine.connect()
engine.execute('use ' + config.db_database)
db = MetaData()


def table_columns(engine, table_name):
    columns = engine.execute('''SELECT column_name
        FROM information_schema.columns
        WHERE table_schema = '{db_name}'
        AND table_name = '{table_name}';'''.format(
        db_name=inspect(engine).default_schema_name,
        table_name=table_name
    )
    )

    return [str(x[0]) for x in columns.fetchall()]


def add_column(engine, table, column, nullable=False, default='null'):
    column_name = column.compile(dialect=engine.dialect)
    column_type = column.type.compile(engine.dialect)
    table_name = table.name
    
    if not nullable and default == 'null':
        if column_type == 'BOOL':
            default = False
        else:
            default = "''"

    if not nullable:
        nullable = 'NOT NULL'
    else:
        nullable = 'NULL'

    if str(column_name) not in table_columns(engine, table_name):
        alterSql = 'ALTER TABLE {table_name} ADD COLUMN {column_name} {type} {nullable} DEFAULT {default}'.format(
            table_name=table_name,
            column_name=column_name,
            type=column_type,
            default=default,
            nullable=nullable
        )

        engine.execute(alterSql)

    table.append_column(column)


def drop_column(engine, table, column_name):
    table_name = table.name
    if column_name in table_columns(engine, table_name):
        engine.execute('ALTER TABLE %s DROP COLUMN %s ' % (table_name, column_name))
