# Python Hakkında
bla bla

# Flask Framework
Php'de ki Slim'e karşılık geliyor diyebiliriz. Aslında tam anlamıyla framework sayılmaz. mikro framework diyebiliriz.
[Documant](https://flask.palletsprojects.com/)
### Proje olusturma

```
mkdir Project
cd Project
```

### Python virtual environment (venv)
```
python3 -m venv venv
source venv/bin/activate
deactivate
```  

Paket yapısı

```
pip freeze > requirements.txt
pip install -r requirements.txt
```

## ilk app :)
```python
from flask import Flask

app = Flask(__name__)
```

## Route
```python
@app.route('/', methods=['GET', 'POST'])
def init():
    pass
```

## Request
```python
argument = request.args.get('name')
parameter = request.form.get('name')
```

## Request
```python
arguments = request.args
argument = request.args.get('name')

parameters = request.form
parameter = request.form.get('name')

files = request.files
file = request.files.get('name')

values = request.values
value = request.values.get('name')
```

## Response 

### Render Template (View)
`app.py`
```python
return render_template('index.html', name='Fatih', surname='İRDAY')
```

OR

```python
data = {
    'name': 'fatih',
    'surname': 'irday'
}
return render_template('index.html', **data)
```
---
`templates/index.html`
```html
{{ name }} <b>{{ surname }}</b>
```


### JSON (API)

```python
return jsonify(dict(name='Fatih')), 200
```

