# Controller
```shell script
mkdir Controllers
cd Controllers
touch HomeController
```

---

`HomeController.py`
```python
from flask import render_template


def home(path):
    # return jsonify(name='path')
    return render_template('index.html', name=path)
```

---

`routes.py`
```python
from Middlewares.Auth import Auth
from Controllers import HomeController


class routes:
    def __init__(self, app):
        app.add_url_rule('/home', view_func=Auth(HomeController.home), methods=['POST'])
```