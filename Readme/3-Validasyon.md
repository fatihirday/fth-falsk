## Validasyon

```shell script
pip install Flask-WTF
mkdir Requests
touch TestRequest 
```

```python
from wtforms import Form, StringField, validators


class TestRequest(Form):
    name = StringField('name', [
        validators.DataRequired(),
        validators.Length(min=4, max=25)
    ])
```

```python
from Requests.TestRequest import TestRequest

@app.route('/valid', methods=['POST'])
def valid():
    form = TestRequest(request.form)

    if not form.validate():
        return jsonify(status='not valid'), 200

    return jsonify(status='valid'), 200
```
