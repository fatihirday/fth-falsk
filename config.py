from dotenv import load_dotenv
import os

env_path = os.getcwd() + '/.env'
load_dotenv(dotenv_path=env_path)


class config:
    auth_key = os.getenv('AUTH_KEY')

    ''' DATABASE '''
    db_connection = os.getenv('DB_CONNECTION')
    db_driver = os.getenv('DB_DRIVER')
    db_host = os.getenv('DB_HOST')
    db_port = os.getenv('DB_PORT')
    db_database = os.getenv('DB_DATABASE')
    db_username = os.getenv('DB_USERNAME')
    db_passpord = os.getenv('DB_PASSWORD')
